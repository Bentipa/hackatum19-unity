﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightController : MonoBehaviour
{

    public int TIME_GREEN = 10;
    public int TIME_RED = 10;
    public int TIME_YELLOW_TO_GREEN = 1;
    public int TIME_YELLOW_TO_RED = 2;

    public Light[] greenLights;
    public Light[] yellowLights;
    public Light[] redLights;

    public TrafficLightController[] opponents;
    public TrafficLightController[] synced;

    private int phase = 2; // 0 = red, 1 = yellow_to_red, 2 = yellow_to_green, 3 = green
    public bool isMasterController = false;
    
    void Start()
    {
        if (isMasterController)
            switchState();     
    }

    private int getOpponentState(int state)
    {
        switch (state)
        {
            case 0:
                return 3;
            case 1:
                return 2;
            case 2:
                return 1;
            case 3:
                return 0;
            default:
                return 0;
        }
    }

    protected void applyState(int state)
    {
        this.phase = state;
        switch (state)
        {
            case 0:
                switchLights(yellowLights, false);
                switchLights(redLights, true);
                break;
            case 1:
            case 2:
                switchLights(greenLights, false);
                switchLights(redLights, false);
                switchLights(yellowLights, true);                
                break;
            case 3:
                switchLights(yellowLights, false);
                switchLights(greenLights, true);
                break;
        }

        foreach(TrafficLightController opp in opponents)
        {
            opp.applyState(getOpponentState(state));
        }

        foreach(TrafficLightController sync in synced)
        {
            sync.applyState(state);
        }
    }

    protected void switchLights(Light[] lights, bool enabled)
    {
        foreach(Light light in lights)
        {
            light.enabled = enabled;
        }
    }

    void switchState()
    {
        Debug.Log("switchState, current=" + phase);
        switch (phase)
        {
            case 0:
                applyState(2);
                Invoke("switchState", TIME_YELLOW_TO_GREEN);
                break;
            case 1:
                applyState(0);
                Invoke("switchState", TIME_RED);
                break;
            case 2:
                applyState(3);
                Invoke("switchState", TIME_GREEN);
                break;
            case 3:
                applyState(1);
                Invoke("switchState", TIME_YELLOW_TO_RED);
                break;
        }
        
    }

    
}
