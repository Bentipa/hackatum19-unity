﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitchingScript : MonoBehaviour
{

    public Camera camera1;
    public Camera camera2;

    bool shownSecond = true;

    public void showSecond()
    {
        shownSecond = true;
        camera1.rect = new Rect(new Vector2(.7f, .7f), new Vector2(1, 1));
        camera1.depth = 1;
        camera2.rect = new Rect(new Vector2(0, 0), new Vector2(1, 1));
        camera2.depth = 0;
    }

    public void showFirst()
    {
        shownSecond = false;
        camera2.rect = new Rect(new Vector2(.7f, .7f), new Vector2(1, 1));
        camera2.depth = 1;
        camera1.rect = new Rect(new Vector2(0, 0), new Vector2(1, 1));
        camera1.depth = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (shownSecond)            
                showFirst();
            else
                showSecond();
        }
    }
}
