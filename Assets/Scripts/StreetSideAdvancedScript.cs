﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StreetSideAdvancedScript : MonoBehaviour
{

    public GameObject[] placeableObjects;
   

    void Start()
    {

        GameObject rendered = transform.GetChild(0).gameObject;
        MeshRenderer renderer = (MeshRenderer)rendered.GetComponent<Renderer>();
        Mesh mesh = rendered.GetComponent<MeshFilter>().mesh;

        for (int i = 0; i < mesh.vertices.Length - 2; i++)
        {
            Vector3 spawnLocation = mesh.vertices[i];
            Quaternion rotation = Quaternion.identity;
            if(i % 3 == 0)
                Instantiate(placeableObjects[0], spawnLocation, rotation);
        } 
    }
}
