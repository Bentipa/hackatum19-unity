﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StreetSideGenScript : MonoBehaviour
{

    
    public bool right = true;
    public bool left = true;

    public GameObject[] objects;

    public float spawnY;
    public int objectCount;
    public float distance;


    // Start is called before the first frame update
    void Start()
    {
        GameObject sideWalk;
        if (right)
        {
            sideWalk = transform.GetChild(0).gameObject;
            PlaceObjects(sideWalk, true);
        }
        if(left)
        {
            sideWalk = transform.GetChild(1).gameObject;
            PlaceObjects(sideWalk, false);
        }

    }

    private void PlaceObjects(GameObject sideWalk, bool rightSide)
    {

        MeshRenderer sideWalkRenderer = sideWalk.GetComponent<MeshRenderer>();
        float xSize = sideWalkRenderer.bounds.size.x;
        float zSize = sideWalkRenderer.bounds.size.z;
        // The direction of the Street (Default onto z-Axis)
        bool xOriented = false;
        if (xSize > zSize)
        {
            xOriented = true;
        }

        Vector3 startLeftPoint;

        if (xOriented)
        {
            Debug.Log("X Oriented!");
            if (rightSide)
            {
                startLeftPoint.z = sideWalkRenderer.bounds.max.z - distance;
            }
            else
            {
                startLeftPoint.z = sideWalkRenderer.bounds.min.z + distance;
            }
            startLeftPoint.x = sideWalkRenderer.bounds.min.x + ((xSize / objectCount) / 2);
        }
        else
        {
            if (rightSide)
            {
                startLeftPoint.x = sideWalkRenderer.bounds.min.x + distance;
            }
            else
            {
                startLeftPoint.x = sideWalkRenderer.bounds.max.x + distance;
            }
            startLeftPoint.z = sideWalkRenderer.bounds.min.z + ((zSize / objectCount) / 2);
        }
        startLeftPoint.y = spawnY;

        int amountObjects = objects.Length;
        int startObject = 0;
        for (int i = 0; i < objectCount; i++)
        {
            GameObject obj = objects[startObject];
            if (!rightSide)
            {
                Instantiate(obj, startLeftPoint, Quaternion.Euler(0, 180, 0));
            }
            else
            {
                Instantiate(obj, startLeftPoint, Quaternion.identity);
            }
            obj.name = "obj_" + i;
            obj.transform.position = startLeftPoint;
            
            if (xOriented)
            {
                startLeftPoint.x += (xSize / objectCount);
            }
            else
            {

                startLeftPoint.z = (xSize / objectCount);
            }
            if (startObject == amountObjects - 1)
            {
                startObject = 0;
            }
            else
            {
                startObject++;
            }
        }
    }
}
